<?php

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('nsctheme_rebuild_registry')) {
  system_rebuild_theme_data();
}

function nsctheme_process_block(&$vars, $hook) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $vars['title'] = $vars['block']->subject;
}

function nsctheme_preprocess_node(&$vars, $hook) {
  if (isset($vars['node_title'])) {
    // $node_title is idiotic. Fixed in Alpha 4.
    $vars['title'] = $vars['node_title'];
  }

  // Special classes for nodes.
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $vars['classes_array'][] = drupal_html_class('node-type-' . $vars['type']);
  if ($vars['promote']) {
    $vars['classes_array'][] = 'node-promoted';
  }
  if ($vars['sticky']) {
    $vars['classes_array'][] = 'node-sticky';
  }
  if (!$vars['status']) {
    $vars['classes_array'][] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
    $vars['classes_array'][] = 'node-by-viewer'; // Node is authored by current user.
  }
  if ($vars['teaser']) {
    $vars['classes_array'][] = 'node-teaser'; // Node is displayed as teaser.
  }
  if (isset($vars['preview'])) {
    $vars['classes_array'][] = 'node-preview';
  }

  $vars['title_attributes_array']['class'][] = 'node-title';
}

function nsctheme_preprocess_page(&$vars, $hook){
	$vars['main_menu_links'] = menu_navigation_links('main-menu');
  // Get the entire main menu tree
  $main_menu_tree = menu_tree_all_data('main-menu');

  // Add the rendered output to the $main_menu_expanded variable
  $vars['main_menu_expanded'] = menu_tree_output($main_menu_tree);
}

function nsctheme_file_icon($variables) {
  $file = $variables['file'];
  $icon_directory = 'sites/all/themes/nsctheme/icons/'; //$variables['icon_directory']

  $mime = check_plain($file->filemime);
  $icon_url = file_icon_url($file, $icon_directory);
  return '<img class="file-icon" alt="" title="' . $mime . '" src="' . $icon_url . '" />';
}

function nsctheme_file_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];

  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  $link_text = "СКАЧАТЬ";
  

  return '<span class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</span>';
}

function nsctheme_css_alter(&$css){
	$path = drupal_get_path('module', 'system');
    unset($css[$path . '/system.theme.css']);
}

function custom_links($links, $separator = null,$attributes = array('class' => 'links')) {
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<ul class="clearfix">';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      $uri_request_id = drupal_get_path_alias($_GET['q']);
      $urlexplode = explode("/", $uri_request_id);
    
     //	  echo $urlexplode[0];
      //echo $link['href'];
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || $link['href'] == $urlexplode[0] ||($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }
      $output .= "</li>";
      if (isset($separator) && $i != $num_links){
      	 $output .= '<li>'.$separator.'</li>';
      }
      $i++;
      $output .= " \n";
    }

    $output .= '</ul>';
  }

  return $output;
}
