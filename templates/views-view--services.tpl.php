<?php
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
<div class="facades clearfix">
    <a class="ventilated-facade"><img src="sites/all/themes/nsctheme/images/facades/ventilated-facade.png" width="195" height="274" alt=""><h4>Вентилируемые фасады</h4>
    <div class="facades-description">
        <h3>На рисунке обозначены:</h3>
        <ul>
          <li>1. Кронштейн для крепления подсистемы</li>
          <li>2. Утеплитель</li>
          <li>3. Тарельчатый дюбель для крепления утеплителя</li>
          <li>4. Подсистема для крепления облицовочного материала</li>
          <li>5. Кляммер</li>
          <li>6. Облицовочный материал (заполнение)</li>
        </ul>
      </div></a>
    <a class="eifs"><img src="sites/all/themes/nsctheme/images/facades/eifs.png" width="196" height="272" alt=""><h4>Мокрые фасады</h4>
      <div class="facades-description">
    <h3>На рисунке обозначены:</h3>
        <ul>
          <li>1. Клей для утеплителя</li>
          <li>2. Утеплитель</li>
          <li>3. Тарельчатый дюбель для крепления утеплителя</li>
          <li>4. Армировочная сетка</li>
          <li>5. Клей для армировочной сетки </li>
          <li>6. Декоративная штукатурка</li>
          <li>7. Краска</li>
        </ul>
      </div>
      </a>
    <a class="glass-facade"><img src="sites/all/themes/nsctheme/images/facades/glass-facade.png" width="251" height="258" style="padding-top: 15px; padding-bottom: 34px;" alt=""><h4>Светопрозрачные конструкции</h4>
    <div class="facades-description">
    <h3>На рисунке обозначены:</h3>
        <ul>
          <li>1. Стойка</li>
          <li>2. Ригель</li>
          <li>3. Термовставка</li>
          <li>4. Стекло (или другое заполнение)</li>
          <li>5. Крышка</li>
        </ul>
      </div>
    </a>
    <div class="numbers">
        <div class="ventilated-facade"></div>
        <div class="eifs"></div>
        <div class="glass-facade"></div>
    </div>
</div>
  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
