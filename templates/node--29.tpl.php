<?php
/**
 * @file
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
?>

    <?php
      print render($content);
    ?>
    <script type="text/javascript">

        jQuery(document).ready( function ($) {

            var myMap;
            var placemarks = [];
            var myCollection ;

            ymaps.ready(init);

        function init () {
            myMap = new ymaps.Map("YMapsID", {
                    center: [59.93772, 30.313622],
                    zoom: 9
                }); /* behaviors: ['drag', 'scrollZoom'] */
                // Первый способ задания метки
                myPlacemark = new ymaps.Placemark([55.8, 37.6]);
                // Второй способ
                myGeoObject = new ymaps.GeoObject({
                    // Геометрия.
                    geometry: {
                        // Тип геометрии - точка
                        type: "Point",
                        // Координаты точки.
                        coordinates: [55.8, 37.8]
                    }
                });

                myMap.controls
                // Кнопка изменения масштаба
                .add('zoomControl')
                // Список типов карты
                .add('typeSelector')
                // Стандартный набор кнопок
                .add('mapTools');

            // Также в метод add можно передать экземпляр класса, реализующего определенный элемент управления.
            // Например, линейка масштаба ('scaleLine')
            myMap.controls
                .add(new ymaps.control.ScaleLine())
                // В конструкторе элемента управления можно задавать расширенные
                // параметры, например, тип карты в обзорной карте
                .add(new ymaps.control.MiniMap({
                    type: 'yandex#publicMap'
                }));

            // Добавляем метки на карту
            myMap.geoObjects
                .add(myPlacemark)
                .add(myGeoObject);

            myCollection = new ymaps.GeoObjectCollection();

            buildPlacemarkers();
            }

            function buildPlacemarkers(){
                $($('.views-row').get().reverse()).each(function(index) {
                    //$(this).find('.lat').html();
                    //$(this).find('.lng').html();
                    var number = $(this).find('.number').html();
                    var title = $(this).find('.title').html();
                    var image = $(this).find('.preview-image').html();
                    var description = $(this).find('.description').html();
                    var current_object;
                    var lat = $(this).find('.lat').html();
                    var lng = $(this).find('.lng').html();
                    var facade_type = $(this).find('.facade-type').html();

                    if ($(this).find('.current_object').html() == "Да") {
                        current_object = "_current";
                    }else{
                        current_object = "";
                    }

                   placemark = new ymaps.Placemark([lat, lng], {iconContent:"<div style = 'font-size: 14px; color: #ffffff;'>" + number + "</div>",
                    balloonContent: '<div class="balloon-content">\
                    ' + image + '<h3>' + title + '</h3><p>' + description + '</p>\
                </div>'}, {
                        // Указываем, что макет иконки будет с контентом
                        iconLayout:"default#imageWithContent",
                        iconImageHref:"http://nsc-fasad.ru/sites/all/themes/nsctheme/images/placemark" + current_object + ".png",
                        iconImageSize:[39, 41], //iconImageSize:[38, 39]
                        iconOffset:[3, 3],
                        // Пиксельный сдвиг содержимого иконки относительно родительского элемента
                        iconContentOffset:[7, 7]
                    });


                    // Добавляет метку на карту

                    myCollection.add(placemark)
                     myMap.geoObjects
                .add(myCollection);
                    placemarks.push(placemark);
                });
            }

            function removePlacemarks(){
                for (var i = 0; i < placemarks.length; i++) {
                    //map.removeOverlay(placemarks[i]);
                     myCollection.removeAll();
                }
            }

             $('body').bind('ajaxSuccess', function(data, status, xhr) {
                //alert('Ajax Success');
                removePlacemarks();
                buildPlacemarkers();
            
            });
        });
  </script>
    <div id="YMapsID"></div>
    <div class="clearfix">
        <span class="notice">* Светло-синим цветом на карте обозначены текущие объекты</span>
    </div>
    <?php
        $viewName = 'gallery';
        print views_embed_view($viewName);
    ?>
