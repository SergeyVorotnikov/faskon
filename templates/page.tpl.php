<?php
/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlight']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */
?>
<div id="wrapper">
<div id="navigation">
    <div class="container clearfix">
        <?php //print custom_links($main_menu_links); ?>
        <?php print render($main_menu_expanded); ?>
    </div>
</div>
<div id="top">
  <div id="header">
    <div class="container clearfix">
      <span id="slogan">Меняя<br/>
облик города</span>
      <div id="logo"><a href="<?php print $front_page; ?>"></a></div>
      <div id="contact">
        <div class="phone clearfix">
            <p class="phone-title">Телефон в Санкт-Петербурге</p>
            <div class="phone-number"><span>8 (812)</span><span class="tel">406-98-60</span></div>
          </div>
          <div class="email">
            <a href="mailto:info@nsc-fasad.ru">info@nsc-fasad.ru</a> / <a href="" class="show-contact">обратная связь</a>
          </div>
      </div>
      </div>
  </div>
  <div id="content">
    <div class="container clearfix">
      <?php print render($page['highlight']); ?>
      <?php print $messages; ?>
      <?php if ($tabs): ?>
        <div class="tabs"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php if ($title && !$is_front): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php if (isset($node) && $node->type == 'object'): ?>
        <a href="/gallery/" class="back-to-gallery">&larr; Назад к галерее</a>
      <?php endif; ?>
      <?php if ($is_front): ?>
      <div class="left-side">
      <?php print render($page['content']); ?>
                <a href="<?php print $front_page; ?>about" class="more">Читать далее</a>
      </div>
      <div class="right-side">
        <h3 class="block-title">ВИДЫ РАБОТ</h3>
        <dl class="facades-homepage ventilated-facade">
          <dt><a href="http://nsc-fasad.ru/services#ventilated-facade"></a></dt>
          <dd><a href="http://nsc-fasad.ru/services#ventilated-facade">Вентилируемые фасады</a></dd>
        </dl>
        <dl class="facades-homepage glass-facade">
          <dt><a href="http://nsc-fasad.ru/services#glass-facade"></a>
          </dt>
          <dd><a href="http://nsc-fasad.ru/services#glass-facade">Светопрозрачные
конструкции</a>
          </dd>
        </dl>
        <dl class="facades-homepage eifs">
          <dt><a href="http://nsc-fasad.ru/services#eifs"></a>
          </dt>
          <dd><a href="http://nsc-fasad.ru/services#eifs">Мокрые фасады</a>
          </dd>
        </dl>
      </div>
      <?php else: ?>
      <?php print render($page['sidebar_first']); ?>
      <?php print render($page['sidebar_second']); ?>
      <?php print render($page['content']); ?>

      <?php endif; ?>
    </div>
  </div>
    </div>
    <?php if ($is_front): ?>
    <div id="homepage-bottom">
      <div class="container clearfix">
        <h3 class="block-title">НАШИ ОБЪЕКТЫ</h3>
        <div id="homepage-gallery">
        <?php
            $viewName = 'nodequeue_1';
            print views_embed_view($viewName);
        ?>
          <p><a href="<?php print $front_page; ?>gallery" class="more">СМОТРЕТЬ ВСЕ</a></p>
        </div>
      </div>
    </div>
    <?php endif; ?>
  <div id="footer">
    <div class="container clearfix">
      <?php print render($page['footer']); ?>
      <p class="copyright">Компания «НСК-Фасады». 198096, Санкт-Петербург, Кронштадтская, д.10, лит. А БЦ «Энигма»<br/>
        Подразделение холдинга <a href="http://nsc-monolit.ru/">«НСК-Монолит»</a>.<br />Все права защищены ©2008-2015.<br/>
       
      </div>  
  </div>
</div>
<?php print render($page['bottom']); ?>