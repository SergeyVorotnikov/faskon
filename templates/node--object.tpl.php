<?php
/**
 * @file
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
?>
<div class="object">
    <?php if (isset($node->field_main_image) && isset($node->field_main_image['und']) && $node->field_main_image['und']):?>
    <img src="<?php print image_style_url('object-main-image', $node->field_main_image['und'][0]['uri']); ?>" width="860" height="370" alt="<?php print $node->field_main_image['und'][0]['title']; ?>" class="object-main-image" title="<?php print $node->field_main_image['und'][0]['title']; ?>" />
    <?php endif; ?> 
    <div class="content">
        <dl class="object-info">
            <?php if (isset($node->field_address['und'])): ?>
            <dt>Адрес:</dt>
            <dd><?php print $node->field_address['und'][0]['value']; ?></dd>
            <?php endif; ?>
            <?php if (isset($node->field_type_of_building['und'])): ?>
            <dt>Тип дома, этажность:</dt>
            <dd><?php print $node->field_type_of_building['und'][0]['value']; ?></dd>
            <?php endif; ?>
            <?php if (isset($node->field_facade['und'])): ?>
            <?php if (isset($node->field_client['und'])): ?>
            <dt>Заказчик:</dt>
            <dd><?php print $node->field_client['und'][0]['value']; ?></dd>
            <?php endif; ?>
            <dt>Вид фасадов:</dt>
            <dd><?php foreach ($node->field_facade['und'] as $key=>$value) {
                $str = substr($value['taxonomy_term']->name, 0, strpos($value['taxonomy_term']->name, ' '));
                print mb_convert_case($str, MB_CASE_TITLE); 
                if (count($node->field_facade['und']) > 1 && $key != count($node->field_facade['und']) - 1){
                    print ', ';
                }
            }?></dd>
            <?php endif; ?>
            <?php if (isset($node->field_area_of_facades['und'])): ?>
            <dt>Площадь фасадов:</dt>
            <dd><?php print $node->field_area_of_facades['und'][0]['value']; ?> м<sup>2</sup></dd>
            <?php endif; ?>
            <?php if (isset($node->field_start_and_completion_date['und'])): ?>
            <dt>Сроки проведения работ:</dt>
            <dd><?php 
            $month = array("января" => "Январь", "Янв" => "Январь", "февраля" => "Февраль", "Фев" => "Февраль", "марта" => "Март", "Мар" => "Март", "апреля" => "Апрель", "Апр" => "Апрель", "мая" => "Май", "Май" => "Май", "июня" => "Июнь", "Июн" => "Июнь", "июля" => "Июль", "Июл" => "Июль", "августа" => "Август", "Авг" => "Август", "сентября" => "Сентябрь", "Сен" => "Сентябрь", "октября" => "Октябрь", "Окт" => "Октябрь", "ноября" => "Ноябрь", "Ноя" => "Ноябрь", "декабря" => "Декабрь", "Дек" => "Декабрь");
            if (isset($node->field_start_and_completion_date['und'][0]['value'])){
                $date = format_date(strtotime($node->field_start_and_completion_date['und'][0]['value']), 'medium', 'M Y');
                print $month[strstr($date, ' ', true)] . ' ' . strstr($date, ' '); 
            } ?>
            <?php endif; ?>
        <?php if (isset($node->field_start_and_completion_date['und'])  && $node->field_start_and_completion_date['und'][0]['value2'] != '' && $node->field_start_and_completion_date['und'][0]['value'] != $node->field_start_and_completion_date['und'][0]['value2']): ?>
            <?php 
            if (isset($node->field_start_and_completion_date['und'][0]['value2'])){
                $date2 = format_date(strtotime($node->field_start_and_completion_date['und'][0]['value2']), 'medium' ,'F Y');
                print ' - ' . $month[strstr($date2, ' ', true)] . ' ' . strstr($date2, ' ');
            } ?></dd>
            <?php endif; ?>
            <?php if (isset($node->field_manufacturer['und'])): ?>
            <dt>Материалы:</dt>
            <dd><?php print $node->field_manufacturer['und'][0]['value']; ?></dd>
            <?php endif; ?>
        </dl>
        <?php print $node->body['und'][0]['value']; ?>
    </div>
    <?php if (isset($node->field_gallery) && isset($node->field_gallery['und']) && $node->field_gallery['und'] && count($node->field_gallery['und']) > 0):?>
    <div class="gallery clearfix">
        <!-- "previous page" action -->
        <a class="prev browse left"></a>
 
        <!-- root element for scrollable -->
        <div class="scrollable" id="scrollable">
 
            <!-- root element for the items -->
            <div class="items">
                <div>
                    <?php foreach ((array)$node->field_gallery["und"] as $key=>$item) { ?>
                    <a href="<?php print image_style_url('big-gallery-image', $item['uri']); ?>" rel="colorbox" class="colorbox"><img src="<?php print image_style_url('gallery-image', $item['uri']); ?>" alt="<?php print $item['title']; ?>" title="<?php print $item['title']; ?>" width="165" height="85" /></a> <!--  title="<?php print $title; ?>"  width="225" height="107" -->
                    <?php if (($key + 1)%4 == 0 && ($key + 1) != count($node->field_gallery["und"])) echo "</div><div>" ?>
                    <?php } ?>
                </div>
            </div>
        </div>
 
        <!-- "next page" action -->
        <a class="next browse right"></a>
        <div class="gallery-bottom"></div>
    </div>
    <?php endif; ?>
   
</div>

